variable "vpc_CIDR_block" {
  type        = string
  description = "VPC CIDR block"
}

variable "destination_CIDR_block" {
  type = string
}

variable "ingress_CIDR_block" {
  type = list
}

variable "default_tags" {
  type=map
}

variable "app_port" {
  default     = 8080
}

variable "app_image" {
}

variable "fargate_cpu" {
  default = 1024
}

variable "fargate_memory" {
  default = 2048
}

variable "app_count" {
  default = 1
}
