# create the VPC
resource "aws_vpc" "vpc" {
  cidr_block            = var.vpc_CIDR_block
  tags                  = merge(
    var.default_tags, {
      "Name" : "phetest-vpc-dev"
      })

}


resource "aws_subnet" "private" {
  count                   = "${length(data.aws_availability_zones.available.names)}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "10.0.${length(data.aws_availability_zones.available.names) + count.index}.0/24"
  map_public_ip_on_launch = false
  availability_zone       = "${element(data.aws_availability_zones.available.names, count.index)}"
  tags                  = merge(
    var.default_tags, {
      "Name" : "phetest-vpc-dev"
      })
}


# create the Public Subnet
resource "aws_subnet" "public" {
  count                   = "${length(data.aws_availability_zones.available.names)}"
  vpc_id                  = "${aws_vpc.vpc.id}"
  cidr_block              = "10.0.${count.index}.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "${element(data.aws_availability_zones.available.names, count.index)}"
  tags                  = merge(
    var.default_tags, {
      "Name" : "phetest-vpc-dev"
      })
}

# create Public VPC Network access control list
resource "aws_network_acl" "public_acl" {
  vpc_id = aws_vpc.vpc.id
  subnet_ids = "${aws_subnet.public.*.id}"
# allow ingress ports
  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destination_CIDR_block
    from_port  = 0
    to_port    = 0
  }

  # allow egress ports
  egress {
    protocol   = -1
    rule_no    = 300
    action     = "allow"
    cidr_block = var.destination_CIDR_block
    from_port  = 0
    to_port    = 0
  }
  tags = merge(
    var.default_tags, {
    "Name" : "phetest-public-acl-dev"
    })
}

resource "aws_security_group" "default" {
  name        = "phe-default"
  vpc_id      = aws_vpc.vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = var.ingress_CIDR_block
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id
}


resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_route" "internet_access" {
  route_table_id            = "aws_route_table.route_table.id"
  destination_cidr_block    = var.destination_CIDR_block
  gateway_id                = aws_internet_gateway.igw.id
}


resource "aws_route_table_association" "association" {
  count         = "${length(data.aws_availability_zones.available.names)}"
  subnet_id      = "{element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "{aws_route_table.route_table.id}"
}
