vpc_CIDR_block = "10.0.0.0/16"

default_tags = {
  Org="PHE"
}

destination_CIDR_block = "0.0.0.0/0"

ingress_CIDR_block = ["0.0.0.0/0"]

app_port = 8080

app_image

fargate_cpu = 1024

fargate_memory = 2048

app_count = 1
